package model;

/**
 * Sebastiaan Aussems
 * 20/09/2022
 */
public class Dog extends Animal {
    public Dog(String name){
        super(name);
    }

    @Override
    public String speak() {
        return "Woof!";
    }
}

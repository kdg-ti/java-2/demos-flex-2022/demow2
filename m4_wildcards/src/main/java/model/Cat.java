package model;

/**
 * Sebastiaan Aussems
 * 20/09/2022
 */
public class Cat extends Animal {
    public Cat(String name) {
        super(name);
    }

    @Override
    public String speak() {
        return "Meaw!";
    }
}

package model;

/**
 * Sebastiaan Aussems
 * 20/09/2022
 *
 * @see: <a href="https://medium.com/omnius/wildcards-in-java-generics-part-1-3-dd2ce5b0e59a</a>
 */
public abstract class Animal {
    private String name;

    protected Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public abstract String speak();
}
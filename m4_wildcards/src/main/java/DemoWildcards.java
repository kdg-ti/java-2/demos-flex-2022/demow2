import model.Cat;
import model.Dog;

import java.util.Arrays;
import java.util.List;

/**
 * Demo voor het gebruik van wildcards in combinatie met generics
 *
 * Sebastiaan Aussems
 * 20/09/2022
 */
public class DemoWildcards {
    public static void main(String[] args) {
        List<Dog> myDogs = Arrays.asList(
                new Dog("Spike"),
                new Dog("Pluto"));
        List<Cat> myCats = Arrays.asList(
                new Cat("Garfield"));

        System.out.println("OK");
    }
}

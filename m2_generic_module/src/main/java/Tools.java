import model.Student;

/**
 * Sebastiaan Aussems
 * 20/09/2022
 */
public class Tools {

    public static void printArray(Integer[] intArray) {
        System.out.print("[");
        for (Integer i : intArray) {
            System.out.print(i + ", ");
        }
        System.out.println("\b\b]");
    }

    public static void printArray(Double[] doubleArray) {
        System.out.print("[");
        for (Double d : doubleArray) {
            System.out.print(d +  ", ");
        }
        System.out.println("\b\b]");
    }

    public static void printArray(String[] strArray) {
        System.out.print("[");
        for (String s : strArray) {
            System.out.print(s + ", ");
        }
        System.out.println("\b\b]");
    }

    public static void printArray(Student[] studArray) {
        System.out.print("[");
        for (Student student : studArray) {
            System.out.print(student + ", ");
        }
        System.out.println("\b\b]");
    }
}
